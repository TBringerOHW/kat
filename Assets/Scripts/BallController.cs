﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;


public class BallController : MonoBehaviour {

    public int sameTypeAmount;
    public int bonusDecreaseAmount, bonusSpeedAmount;

    public bool isLevelComplete;

    public float speedTimer, magnetTimer, sizeTimer, controlInversionTimer;
    public float ballSize;

    private ObjectType lastObjectType;
    private ControlManager controlManager;
    private bool isSpeedBonusActive, isMagnetActive, isSizeChangingActive, isControlInverted;
    private SoundController sound;

    void Start() {
        controlManager = GetComponent<ControlManager>();
        sound = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>();
        ballSize = transform.localScale.x;
    }

    void Update() {
        if (isSpeedBonusActive || isMagnetActive || isSizeChangingActive || isControlInverted) {
            Timers();
        }
    }

    public void IncreaseSizeTimer() {
        sizeTimer += Time.deltaTime;
    }

    public bool IsSizeChangingActive() {
        return isSizeChangingActive;
    }

    private void Timers() {

        if (isSpeedBonusActive) {
            LevelHelper.instance.EffectIcon("speedUp", speedTimer);
            speedTimer -= Time.deltaTime;
            if (speedTimer <= 0) {
                LevelHelper.instance.EffectIcon("speedUp", 0);
                isSpeedBonusActive = false;
                speedTimer = 0;
                controlManager.isSpeedBonusActive = false;
            }
        }

        if (isMagnetActive) {
            LevelHelper.instance.EffectIcon("magnet", magnetTimer);
            magnetTimer -= Time.deltaTime;
            if (magnetTimer <= 0) {
                LevelHelper.instance.EffectIcon("magnet", 0);
                isMagnetActive = false;
                magnetTimer = 0;
            }
        }

        if (isSizeChangingActive) {
            LevelHelper.instance.EffectIcon("size", sizeTimer);
            sizeTimer -= Time.deltaTime;
            if (sizeTimer <= 0) {
                LevelHelper.instance.EffectIcon("size", 0);
                isSizeChangingActive = false;
                sizeTimer = 0;
                transform.localScale = Vector3.one * ballSize;
                controlManager.isDecreased = false;
            }
        }

        if (isControlInverted) {
            LevelHelper.instance.EffectIcon("invert", controlInversionTimer);
            controlInversionTimer -= Time.deltaTime;
            if (controlInversionTimer <= 0) {
                LevelHelper.instance.EffectIcon("invert", 0);
                controlManager.isControlInverted = false;
                isControlInverted = false;
                controlInversionTimer = 0;
            }
        }

    }


    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<InteractiveObject>() && !LevelHelper.instance.isGameOver) {
            TakeObject(other.GetComponent<InteractiveObject>());
            Destroy(other.gameObject);
        }
    }

    private void TakeObject(InteractiveObject obj) {
        lastObjectType = obj.type;
        sound.PlaySound("pickUp");
        if (obj.type == lastObjectType) {
            sameTypeAmount++;
        }
        switch (obj.type) {

            case ObjectType.Common: {
                    LevelHelper.instance.ChangeScore(sameTypeAmount);
                    break;
                }

            case ObjectType.SpeedUp: {
                    bonusSpeedAmount++;
                    bonusDecreaseAmount = 0;
                    if (bonusSpeedAmount == 3) {
                        bonusSpeedAmount = 0;
                        ApplySpeedBonus();
                    }
                    if (isMagnetActive) {
                        LevelHelper.instance.ChangeScore(sameTypeAmount);
                    }
                    else {
                        LevelHelper.instance.ChangeScore(1);
                    }
                    break;
                }

            case ObjectType.Decreaser: {
                    bonusDecreaseAmount++;
                    bonusSpeedAmount = 0;
                    if (bonusDecreaseAmount == 3) {
                        bonusDecreaseAmount = 0;
                        DecreaseSize();
                    }
                    if (isMagnetActive) {
                        LevelHelper.instance.ChangeScore(sameTypeAmount);
                    }
                    else {
                        LevelHelper.instance.ChangeScore(1);
                    }
                    break;
                }

            case ObjectType.WorldEater: {
                    ApplyMagnet();
                    if (isMagnetActive) {
                        LevelHelper.instance.ChangeScore(sameTypeAmount);
                    }
                    else {
                        LevelHelper.instance.ChangeScore(1);
                    }
                    break;
                }

            case ObjectType.Invertor: {
                    InvertControl();
                    sameTypeAmount = 0;
                    LevelHelper.instance.ChangeScore(1);
                    //Sound
                    sound.PlaySound("streakEnd");
                    break;
                }

            case ObjectType.ScoreReducer: {
                    sameTypeAmount = 0;
                    LevelHelper.instance.ChangeScore(-1);
                    //Sound
                    sound.PlaySound("streakEnd");
                    break;
                }

            default: 
                return;
        }

    }

    private void ApplySpeedBonus() {
        speedTimer += 12f;
        isSpeedBonusActive = true;
        LevelHelper.instance.EffectPopup("speedBonus");
        controlManager.isSpeedBonusActive = true;
    }

    private void DecreaseSize() {
        sizeTimer += 15f;
        isSizeChangingActive = true;
        LevelHelper.instance.EffectPopup("size");
        transform.localScale = Vector3.one * ballSize / 4;
        controlManager.isDecreased = true;
    }

    private void ApplyMagnet() {
        magnetTimer += 25f;
        isMagnetActive = true;
        LevelHelper.instance.EffectPopup("magnet");
    }

    private void InvertControl() {
        controlManager.isControlInverted = true;
        isControlInverted = true;
        LevelHelper.instance.EffectPopup("invert");
        controlInversionTimer += 10f;
    }

}
