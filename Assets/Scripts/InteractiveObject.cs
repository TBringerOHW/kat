﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveObject : MonoBehaviour {

    public ObjectType type;
}

public enum ObjectType {
    Nothing,
    Common,
    SpeedUp,
    Decreaser,
    WorldEater,
    Invertor,
    ScoreReducer
}
