﻿using UnityEngine;

public class CameraControl : MonoBehaviour {
    [Range(0, 2)]
    public float posY = 0.784f;
    [Range(-2, 0)]
    public float posZ = -1.057f;

    public float test;

    public Transform cameraCorrection;
    public float smooth = 3f;
    
    public Transform normalPosition;
    public static Transform positionCorrection;
    public static float targetHeight;

    private Vector3 curPos;
    private BallController ball;

    void Start()
    {
        transform.position = normalPosition.position;
        transform.forward = normalPosition.forward;
        ball = GameObject.FindGameObjectWithTag("Player").GetComponent<BallController>();
        NormalView();
    }

    void FixedUpdate()
    {
        NormalView();
        UpdateCameraDistance();
    }

    void Result()
    {
        transform.position = Vector3.Lerp(transform.position, cameraCorrection.position, Time.fixedDeltaTime * smooth);
        transform.forward = Vector3.Lerp(transform.forward, cameraCorrection.forward, Time.fixedDeltaTime * smooth);
    }

    void NormalView()
    {
        positionCorrection = normalPosition;
        targetHeight = normalPosition.position.y;
        Result();

    }


    public void UpdateCameraDistance() {

		float percentChange = ball.transform.localScale.x;
        /*test = percentChange;
        normalPosition.localPosition = new Vector3(0f, posY * percentChange, -0.8456f * percentChange);*/

        normalPosition.localPosition = new Vector3(0f, posY * percentChange, posZ * percentChange);
    }
}
