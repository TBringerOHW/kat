﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Xml.Linq;
using System.IO;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;
    public bool isMusicOn;
    public bool isFxOn;
    public int inputType;
    public bool isTutorialEnded;

    public string previousScene;
    public string currentScene;

    private string savePath;
    private GameObject loading;

    AsyncOperation async;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
        savePath = Application.persistentDataPath + "/save.xml";
        currentScene = SceneManager.GetActiveScene().name;
        LoadSettings();
        if (currentScene == "Boot")
            StartCoroutine(InitGame());
        if (previousScene == "" && currentScene != "Boot")
            Instantiate((GameObject)Resources.Load("Prefabs/ConfigManager", typeof(GameObject)));

    }

    private void OnLevelWasLoaded(int level)
    {
        if (level != 0 && level !=1)
        {
            Instantiate((GameObject)Resources.Load("Prefabs/ConfigManager", typeof(GameObject)));
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadScene(string name)
    {
        previousScene = currentScene;
		currentScene = name;
        StartCoroutine("Load");
    }

    private IEnumerator Load()
    {
        async = SceneManager.LoadSceneAsync(currentScene);
        if (async.isDone)
        {
            Debug.Log("ss");
            yield return async;
        }
    }

    void LoadSettings()
    {
        XElement settings;

        if (File.Exists(savePath))
        {
            settings = XDocument.Parse(File.ReadAllText(savePath)).Element("settings");

        }
        else
        {
            settings = XDocument.Parse(((TextAsset)Resources.Load("Configs/saveDefault")).text).Element("settings");
        }
        isMusicOn = (settings.Element("isMusicOn").Value == "true" ? true : false);
        isFxOn = (settings.Element("isFxOn").Value == "true" ? true : false);
        inputType = (settings.Element("inputType").Value == "1" ? 1 : 0);
        isTutorialEnded = (settings.Element("isTutorialEnded").Value == "true" ? true : false);
    }

    public void SaveSettings()
    {
        XElement settings = new XElement("settings");
        settings.Add(new XElement("isMusicOn", isMusicOn));
        settings.Add(new XElement("isFxOn", isFxOn));
        settings.Add(new XElement("inputType", inputType));
        settings.Add(new XElement("isTutorialEnded", isTutorialEnded));

        XDocument saveXml = new XDocument(settings);
        File.WriteAllText(savePath, saveXml.ToString());
    }

    IEnumerator InitGame()
    {
        loading = GameObject.Find("Loading");
        loading.SetActive(false);
        yield return new WaitForSeconds(2);
        loading.SetActive(true);
        if (isTutorialEnded)
        {
            LoadScene("MainMenu");
        }
        else
        {
            LoadScene("MainMenu");
        }
    }
}
