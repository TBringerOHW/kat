﻿using UnityEngine;

public class HideBall : MonoBehaviour {

    private GameObject player;
    public float rotateSmooth = 30f;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public void Rotate(float h)
    {
        transform.Rotate(new Vector3(0, rotateSmooth * h, 0));
    }

	void Update () {
        transform.position = player.transform.position;
	}
}
