﻿using UnityEngine;

public class TriggerDetection : MonoBehaviour {

    public int dialogId;

    private bool isEntererd;

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !isEntererd)
        {
            isEntererd = true;
            LevelHelper.instance.GetDialog(dialogId);
        }
    }
}
