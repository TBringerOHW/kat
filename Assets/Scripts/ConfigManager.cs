﻿using UnityEngine;
using System.Linq;
using System.Xml.Linq;

public class ConfigManager : MonoBehaviour {

    private XElement dialogs;
    private XElement levelSettings;

    public float levelTime;
    public int levelScore;
    public bool isDialogOnStart;

	void Awake() {
        dialogs = XDocument.Parse(((TextAsset)Resources.Load("Configs/dialogs")).text).Element("root").Element(GameManager.instance.currentScene);
        levelSettings = XDocument.Parse(((TextAsset)Resources.Load("Configs/levelSettings")).text).Element("root").Element(GameManager.instance.currentScene);

        levelTime = float.Parse(levelSettings.Element("time").Value);
        levelScore = int.Parse(levelSettings.Element("score").Value);
        isDialogOnStart = (levelSettings.Element("isDialogOnStart").Value == "true" ? true : false);
    }

    public string GetDialog(int id)
    {
        var elements = from xe in dialogs.Elements("dialog")
                        where xe.Attribute("id") != null
                     select xe;
        foreach (var element in elements)
        {
            if (element.Attribute("id").Value == id.ToString())
            {
                return element.Value;
            }
        }
        return null;
    }

    public int NextDialog(int id)
    {
        var elements = from xe in dialogs.Elements("dialog")
                       where xe.Attribute("id") != null
                       select xe;
        foreach (var element in elements)
        {
            if (element.Attribute("id").Value == id.ToString())
            {
                if (element.Attribute("next") != null)
                {
                    return int.Parse(element.Attribute("next").Value);
                }
            }
        }
        return 0;
    }
}
