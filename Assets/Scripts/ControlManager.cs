﻿using UnityEngine;
using UnityStandardAssets.Vehicles.Ball;

public class ControlManager : MonoBehaviour {

    public bool isDebug;
    public bool isControlInverted, isSpeedBonusActive, isDecreased;
    public float speedBoost, decreaseSpeedUp;
    public Vector3 yPowerComponent;

    private BallMovement ball;
    private HideBall hideBall;

    private Vector3 move;
    private Vector3 camForward;

    private Transform cam;

    float lateralDirection;
    float forwardDirection;
    float rotation;

    private void Awake()
    {
        ball = GameObject.FindGameObjectWithTag("Player").GetComponent<BallMovement>();
        hideBall = GameObject.FindGameObjectWithTag("HideBall").GetComponent<HideBall>();
        cam = Camera.main.transform;
    }

    private void Update () {
        lateralDirection = Input.GetAxis("Horizontal") * (isControlInverted ? -1 : 1) + (isControlInverted ? -1 : 1) * (GameManager.instance.inputType == 0 ? rotation : Input.acceleration.x) ;
        forwardDirection = Input.GetAxis("Vertical");

        //camForward = Vector3.Scale(cam.forward, new Vector3(1, 0, 1)).normalized;
        camForward = Vector3.Scale(hideBall.transform.forward, new Vector3(1, 0f, 1)).normalized + yPowerComponent;
        Debug.DrawLine(transform.position, transform.position + camForward * 2, Color.red);
        move = ((isDebug?forwardDirection:1f) * camForward + (isDebug ? 0 : lateralDirection) * 3f * cam.right).normalized * ((LevelHelper.instance.ball.ballSize/2f)
            + ( (isSpeedBonusActive ? speedBoost : 0f) + (isDecreased ? decreaseSpeedUp : 0f) ) * ((isSpeedBonusActive && isDecreased) ? 1 : .65f));
    }

    private void FixedUpdate()
    {
        ball.Move(move);
        if (lateralDirection != 0)
        {
            hideBall.Rotate(lateralDirection);
        }            

    }

    public void rotationByTouch(int direction)
    {
        rotation = direction;
    }
}
