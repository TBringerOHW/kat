﻿using UnityEngine;
using UnityEngine.UI;

public class LevelHelper : MonoBehaviour {

    public static LevelHelper instance;
    [HideInInspector] public BallController ball;
    [HideInInspector] public bool isGameOver;

    private GameObject timePanel;
    private GameObject scorePanel;
    private GameObject pausePanel;
    private GameObject dialogPanel;
    private GameObject gameOverPanel;

    private GameObject dialogText;
    private GameObject scoreText;
    private GameObject effectText;
    private GameObject loadingScreen;

    private GameObject speedUpIcon;
    private GameObject magnetIcon;
    private GameObject sizeIcon;
    private GameObject invertIcon;

    private float levelTime;
    private int score;
    private int levelScore;
    private float timer;
    private bool isPause;

    private int minutes;
    private string seconds;
    private int sec;

    private int dialogCount;

    private ControlManager controlManager;
    private ConfigManager configManager;

    private SoundController sound;

    void Start() {
        instance = this;    
        ball = GameObject.FindGameObjectWithTag("Player").GetComponent<BallController>();
        controlManager = GameObject.FindGameObjectWithTag("Player").GetComponent<ControlManager>();
        configManager = GameObject.FindObjectOfType<ConfigManager>();
        sound = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>();
        timePanel = GameObject.Find("TimePanel");
        scorePanel = GameObject.Find("ScorePanel");

        pausePanel = GameObject.Find("PausePanel");
        pausePanel.SetActive(false);

        dialogPanel = GameObject.Find("DialogPanel");
        dialogText = GameObject.Find("DialogPanel/DialogWindow/DialogText");
        dialogPanel.SetActive(false);

        gameOverPanel = GameObject.Find("GameOverPanel");
        gameOverPanel.SetActive(false);

        scoreText = (GameObject)Resources.Load("Prefabs/ScoreText", typeof(GameObject));

        effectText = (GameObject)Resources.Load("Prefabs/EffectText", typeof(GameObject));

        speedUpIcon = GameObject.Find("IconBarLeft/speedUp");
        speedUpIcon.SetActive(false);

        magnetIcon = GameObject.Find("IconBarLeft/magnet");
        magnetIcon.SetActive(false);

        sizeIcon = GameObject.Find("IconBarRight/size");
        sizeIcon.SetActive(false);

        invertIcon = GameObject.Find("IconBarRight/invert");
        invertIcon.SetActive(false);

        loadingScreen = GameObject.Find("LoadingScreen");
        loadingScreen.SetActive(false);

        levelTime = configManager.levelTime;
        levelScore = configManager.levelScore;
        score = 0;
        isGameOver = false;
        timer = levelTime;
        dialogCount = 1;
        if (configManager.isDialogOnStart)
        {
            PauseGame();
            scorePanel.SetActive(false);
            timePanel.SetActive(false);
            dialogPanel.SetActive(true);
            dialogText.GetComponent<Text>().text = configManager.GetDialog(dialogCount);
        }
        else
            ResumeGame();
        SetTime();
    }


    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPause)
            {
                PauseGame();
                pausePanel.SetActive(true);
            } else
            {
                ResumeGame();
            }
        }
        if (timer > 0 && !isPause)
        {
            timer -= Time.deltaTime;
            SetTime();
            if (minutes == 0 && sec <= 31)
                timePanel.GetComponentInChildren<Text>().GetComponent<Animator>().SetTrigger("pulse");
        }
        else if (!isPause && !isGameOver)
        {
            GameOver("time out");
        }
        if (ball.isLevelComplete == true && !isGameOver)
        {
            GameOver("level complete");
        }
    }

    public void ChangeScore(int value)
    {
        score = score + value;
        scorePanel.GetComponentInChildren<Text>().text = "" + score;
        scorePanel.GetComponentInChildren<Image>().transform.localScale = new Vector3((float) score / levelScore * 150, (float) score / levelScore * 150, 1);
        ScorePopup(value);
        if (score >= levelScore && !isGameOver)
        {
            if (GameManager.instance.currentScene != "Tutorial")
                GameOver("level complete");
            else
                GameOver("tutorial complete");
        }
    }

    public void ScorePopup(int value)
    {
        GameObject temp = Instantiate(scoreText) as GameObject;
        RectTransform tempRect = temp.GetComponent<RectTransform>();
        temp.transform.SetParent(GameObject.Find("Canvas").GetComponent<Transform>());
        tempRect.transform.localPosition = scoreText.transform.localPosition;
        tempRect.transform.localScale = scoreText.transform.localScale;
        tempRect.transform.localRotation = scoreText.transform.localRotation;

        if (value != 0)
        {
            if (value > 0)
                temp.GetComponent<Text>().text = "+" + value;
            else
                temp.GetComponent<Text>().text = "" + value;
        }
        temp.GetComponent<Animator>().SetTrigger("Enter");
        Destroy(temp, 0.7f);
    }

    public void EffectPopup(string name)
    {
        GameObject temp = Instantiate(effectText) as GameObject;
        RectTransform tempRect = temp.GetComponent<RectTransform>();
        temp.transform.SetParent(GameObject.Find("Canvas").GetComponent<Transform>());
        tempRect.transform.localPosition = scoreText.transform.localPosition;
        tempRect.transform.localScale = scoreText.transform.localScale;
        tempRect.transform.localRotation = scoreText.transform.localRotation;

        switch (name)
        {
            case "speedBonus":
                temp.GetComponent<Text>().text = "Ускорение!";
                break;
            case "magnet":
                temp.GetComponent<Text>().text = "Магнит активирован!";
                break;
            case "size":
                temp.GetComponent<Text>().text = "Размер уменьшен!";
                break;
            case "invert":
                temp.GetComponent<Text>().text = "Инверсия управления!";
                break;
        }

        Destroy(temp, 0.7f);
    }

    public void EffectIcon (string name, float time)
    {
        //Debug.Log(name + " " + time);
        switch (name)
        {
            case "speedUp":
                if (time > 0 && !isGameOver)
                {
                    speedUpIcon.SetActive(true);
                    speedUpIcon.GetComponentInChildren<Text>().text = "" + (int)time;
                }
                else
                    speedUpIcon.SetActive(false);
                break;
            case "magnet":
                if (time > 0 && !isGameOver)
                {
                    magnetIcon.SetActive(true);
                    magnetIcon.GetComponentInChildren<Text>().text = "" + (int)time;
                }
                else
                    magnetIcon.SetActive(false);
                break;
            case "size":
                if (time > 0 && !isGameOver)
                {
                    sizeIcon.SetActive(true);
                    sizeIcon.GetComponentInChildren<Text>().text = "" + (int)time;
                }
                else
                    sizeIcon.SetActive(false);
                break;
            case "invert":
                if (time > 0 && !isGameOver)
                {
                    invertIcon.SetActive(true);
                    invertIcon.GetComponentInChildren<Text>().text = "" + (int)time;
                }
                else
                    invertIcon.SetActive(false);
                break;
        }
    }

    public void GetDialog(int id)
    {
        dialogCount = id;
        PauseGame();
        dialogPanel.SetActive(true);
        dialogText.GetComponent<Text>().text = configManager.GetDialog(dialogCount);
    }

    public void NextDialog()
    {
        int next = configManager.NextDialog(dialogCount);
        if (next != 0)
        {
            dialogCount = next;
            dialogText.GetComponent<Text>().text = configManager.GetDialog(dialogCount);
        }
        else
        {
            scorePanel.SetActive(true);
            timePanel.SetActive(true);
            ResumeGame();
        }
    }

    private void SetTime()
    {
        minutes = (int)timer / 60;
        sec = (int)timer % 60;
        seconds = "" + ((sec > 9) ? "" + sec : "0" + sec);
        timePanel.GetComponentInChildren<Text>().text = "" + minutes + ":" + seconds;
        timePanel.GetComponentInChildren<Image>().fillAmount = timer / levelTime;
    }

    private void PauseGame()
    {
        sound.PlaySound("ButtonClick");
        Time.timeScale = 0;
        isPause = true;
    }

    private void ResumeGame()
    {
        sound.PlaySound("ButtonClick");
        Time.timeScale = 1;
        dialogPanel.SetActive(false);
        pausePanel.SetActive(false);
        isPause = false;
    }

    public void ButtonClicked(string button)
    {
        switch (button)
        {
            case "restart":
                LoadLevel(GameManager.instance.currentScene);
                break;
            case "menu":
                LoadLevel("MainMenu");
                break;
            case "continue":
                ResumeGame();
                break;
        }
    }

    private void GameOver(string reason)
    {
        isGameOver = true;
        Time.timeScale = 0;
        timePanel.SetActive(false);
        scorePanel.SetActive(false);
        //controlManager.isGameOver = true;
        gameOverPanel.SetActive(true);
        switch (reason)
        {
            case "time out":
                gameOverPanel.GetComponentInChildren<Text>().text = "Время вышло!\n Счёт: " + score;
                gameOverPanel.GetComponent<Image>().color = new Color(255, 0, 0, 0.2f);
                sound.PlaySound("levelLose");
                break;
            case "level complete":
                gameOverPanel.GetComponentInChildren<Text>().text = "Уровень пройден!";
                gameOverPanel.GetComponent<Image>().color = new Color(0, 255, 0, 0.2f);
                sound.PlaySound("levelWin");
                break;
            case "tutorial complete":
                gameOverPanel.GetComponentInChildren<Text>().text = "Обучение пройдено!";
                gameOverPanel.GetComponent<Image>().color = new Color(0, 255, 0, 0.2f);
                sound.PlaySound("levelWin");
                break;
        }
    }

    private void LoadLevel(string name)
    {
        sound.PlaySound("ButtonClick");
        loadingScreen.SetActive(true);
        GameManager.instance.LoadScene(name);
    }
}
