﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuHelper : MonoBehaviour {

    Stack<GameObject> path = new Stack<GameObject>();

    public GameObject currentMenu;

    private Toggle optionsToggle;
    private Toggle music;
    private Toggle fx;
    private Toggle input;

    private GameObject mainMenu;
    private GameObject exitPanel;
    private GameObject levelSelector;
    private GameObject loadingScreen;
    private GameObject bootScreen;

    private SoundController sound;

    void Awake()
    {
        sound = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundController>();
        bootScreen = GameObject.Find("BootScreen");
        if (GameManager.instance.previousScene != "Boot")
            bootScreen.SetActive(false);

        mainMenu = GameObject.Find("MainMenu");

        exitPanel = GameObject.Find("ExitPanel");
        exitPanel.SetActive(false);

        levelSelector = GameObject.Find("LevelSelector");
        levelSelector.SetActive(false);

        loadingScreen = GameObject.Find("LoadingScreen");
        loadingScreen.SetActive(false);

        optionsToggle = GameObject.Find("MainMenu/OptionsButton").GetComponent<Toggle>();

        music = GameObject.Find("MainMenu/OptionsButton/Music").GetComponent<Toggle>();
        fx = GameObject.Find("MainMenu/OptionsButton/FX").GetComponent<Toggle>();
        input = GameObject.Find("MainMenu/OptionsButton/Input").GetComponent<Toggle>();

        music.isOn = GameManager.instance.isMusicOn;
        fx.isOn = GameManager.instance.isFxOn;
        input.isOn = GameManager.instance.inputType == 1 ? true : false;
        currentMenu = GameObject.FindGameObjectWithTag("Menu");

        bootScreen.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackMenu();
        }
    }

    public void TogglePressed(string name)
    {
        Debug.Log("BtnPressed");
        sound.PlaySound("ButtonClick");
        switch (name)
        {
            case "music":
                GameManager.instance.isMusicOn = music.isOn;
                GameManager.instance.SaveSettings();
                break;
            case "fx":
                GameManager.instance.isFxOn = fx.isOn;
                GameManager.instance.SaveSettings();
                break;
            case "input":
                if (input.isOn)
                    GameManager.instance.inputType = 1;
                else
                    GameManager.instance.inputType = 0;
                GameManager.instance.SaveSettings();
                break;
            case "options":
                if (optionsToggle.isOn)
                {
                    MenuSwitcher("Options");
                    optionsToggle.GetComponent<Animator>().SetBool("IsOn", true);
                }
                else
                {
                    optionsToggle.GetComponent<Animator>().SetBool("IsOn", false);
                }
                break;
        }
    }

    public void QuitGame()
    {
        GameManager.instance.QuitGame();
    }

    public void LoadLevel(string name)
    {
        loadingScreen.SetActive(true);
        GameManager.instance.LoadScene(name);
    }

    void BackMenu()
    {
        if (path.Count == 0)
        {
            MenuSwitcher("ExitPanel");
            return;
        }
        currentMenu.SetActive(false);
        currentMenu = path.Pop();
        currentMenu.SetActive(true);
    }

	public void MenuSwitcher (string name) 
	{
        sound.PlaySound("ButtonClick");
        GameObject to;
        switch (name)
        {
            case "MainMenu":
                currentMenu.SetActive(false);
                to = mainMenu;
                break;
            case "ExitPanel":
                optionsToggle.isOn = false;
                to = exitPanel;
                break;
            case "LevelSelector":
                currentMenu.SetActive(false);
                to = levelSelector;
                break;
            default:
                to = mainMenu;
                break;
        }
        path.Push(currentMenu);
        //currentMenu.SetActive(false);
		to.SetActive(true);
        currentMenu = to;
        if (name == "MainMenu")
            path.Clear();
    }
}
