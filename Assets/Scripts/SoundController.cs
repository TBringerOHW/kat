﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour {

    public AudioClip pickUpSound, streakEndSound, buttonClickSound, levelWinSound, levelLoseSound;
    private AudioSource audioSource;

	// Use this for initialization
	void Awake () {
        audioSource = GetComponent<AudioSource>();
	}

    public void PlaySound(string soundName) {
        switch (soundName) {
            case "pickUp": {
                    audioSource.clip = pickUpSound;
                    break;
                }
            case "streakEnd": {
                    audioSource.clip = streakEndSound;
                    break;
                }
            case "ButtonClick": {
                    audioSource.clip = buttonClickSound;
                    break;
                }
            case "levelWin": {
                    audioSource.clip = levelWinSound;
                    break;
                }
            case "levelLose": {
                    audioSource.clip = levelLoseSound;
                    break;
                }
        }

        audioSource.Play();
    }
}
