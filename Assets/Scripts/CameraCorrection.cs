﻿using UnityEngine;

public class CameraCorrection : MonoBehaviour {

    public Transform target;
    public float distanceOffset = 0.25f;

    void Update()
    {
        transform.position = CameraControl.positionCorrection.position;
        transform.forward = CameraControl.positionCorrection.forward;
        RaycastHit hit;
        Vector3 trueTargetPosition = target.transform.position;
        Debug.DrawLine(trueTargetPosition, transform.position, Color.blue);
        if (Physics.Linecast(trueTargetPosition, transform.position, out hit))
        {
            if (LevelHelper.instance.ball.IsSizeChangingActive()) {
                LevelHelper.instance.ball.IncreaseSizeTimer();
            }
            float tempDistance = Vector3.Distance(trueTargetPosition, hit.point);
            Vector3 position = target.position - (transform.rotation * Vector3.forward * (tempDistance - distanceOffset));
            transform.position = new Vector3(position.x, CameraControl.targetHeight, position.z);
            transform.LookAt(target);
        }
    }
}
